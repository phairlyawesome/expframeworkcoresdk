'''
Imagine the script below was a common data step across projects that we should integrate into our SDK

STEP 1: Refactor the functionality below into a module title TopStores
    Use a single class in your module
    Initialize the class with all necessary variables

STEP 2: Build that module into a wheel file
'''

import json

import pandas as pd

store_data_path = 'data/store_data.json'
metric = 'sales'
top_n = 3


def parse_json(file_path: str) -> pd.DataFrame:
    with open(file_path, 'r') as file:
        store_data_dict: dict = json.loads(file.read())
    store_data_local = pd.DataFrame.from_dict(store_data_dict)
    return store_data_local


def fetch_top_n_stores(store_data: pd.DataFrame, metric: str, n: int) -> pd.DataFrame:
    top_n_stores: pd.DataFrame = store_data.groupby('store_nbr') \
        .agg({metric: 'sum'}) \
        .reset_index() \
        .sort_values('sales', ascending=False) \
        .head(n)
    return top_n_stores


store_data: pd.DataFrame = parse_json(file_path=store_data_path)

top_n_stores: pd.DataFrame = fetch_top_n_stores(store_data=store_data, metric=metric, n=top_n)
