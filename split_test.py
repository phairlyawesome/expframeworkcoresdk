'''
STEP 1: Read and explore data

Read in the file titled 'test_results.pkl' in the 'data' directory.
This file is a pickled pandas data frame saved with 'gzip' compression

It contains the following columns:
    order_id
    model_version
    order_date
    customer_id
    basket_size_dollars

STEP 2: Evaluate performance

Which model performed the best?
Are the results significant?
What other models might you consider using?

'''

